import bnf
from collections import OrderedDict, namedtuple


Event = namedtuple("Event", "type src dst flair text")


class Object:
    def __init__(self, name, text, index):
        self.name = name
        self.text = text
        self.index = index


objects = OrderedDict()
events = []
aliases = {}

def lookup_alias(name):
    if name not in aliases:
        if name in objects:
            return name
        raise bnf.ParseError("Alias target not an object and not an alias: '{}'".format(name))
    return lookup_alias(aliases[name][-1])

def lookup_object(name):
    return objects[lookup_alias(name)]

def cb_object_stmt(name, text):
    objects[name] = Object(name, text, len(objects))

def cb_alias_object_stmt(alias, name):
    actual_name = lookup_alias(name)
    aliases.setdefault(alias, []).append(actual_name)

def cb_unalias_object_stmt(alias):
    try:
        aliases[alias].pop()
        if not aliases[alias]:
            del aliases[alias]
    except KeyError:
        pass

def cb_event_flair_stmt(name, src, dst, flairy_text):
    flair, text = flairy_text
    if dst in ("-", "."):
        dst = src
    if src == dst and src in ("-", "."):
        srcobj, dstobj = None, None
    else:
        try:
            srcobj, dstobj = lookup_object(src), lookup_object(dst)
        except KeyError as ex:
            raise bnf.ParseError("Referenced unknown object '{}' in event ({}, {}, {}, {})".format(ex.args[0], name, src, dst, flairy_text))
    events.append(Event(name, srcobj, dstobj, flair, text))


scannerspec = OrderedDict((
    ("nl", r"\r?\n"),
    ("ws", r"\s+"),
    ("lbracket", r"\["),
    ("rbracket", r"\]"),
    ("hash", r"#"),
    ("alias-object", r"alias-object"),
    ("unalias-object", r"unalias-object"),
    ("object", r"object"),
    ("id", r"[\w\-.]+"),
    ("word", r"\S+"),
))

rulespecs = {
    "<spec>": [
        ("<line_list>", None),
    ],
    "<line_list>": [
        ("nl <line_list>", None),
        ("ws <line_list>", None),
        ("<line> <line_list>", None),
        ("@", None),
    ],
    "<line>": [
        ("hash <text>", None),
        ("<statement>", None),
    ],
    "<statement>": [
        ("<object_stmt>", None),
        ("<alias_object_stmt>", None),
        ("<unalias_object_stmt>", None),
        ("<event_stmt>", None),
    ],
    "<object_stmt>": [
        ("(object) (ws) id (ws) <text>", cb_object_stmt),
    ],
    "<alias_object_stmt>": [
        ("(alias-object) (ws) id (ws) <text>", cb_alias_object_stmt),
    ],
    "<unalias_object_stmt>": [
        ("(unalias-object) (ws) id", cb_unalias_object_stmt),
    ],
    "<event_stmt>": [
        ("id (ws) id (ws) id (ws) <flairy_text>", cb_event_flair_stmt),
    ],
    "<flairy_text>": [
        ("<flair> (ws) <text>", None),
        ("<text>", lambda x,: (None, x)),
    ],
    "<flair>": [
        ("(lbracket) id (rbracket)", lambda xs: xs),
    ],
    "<text>": [
        ("<text_item> <text_cont>", lambda x, xs: x + xs),
    ],
    "<text_item>": [
        ("word", lambda x: x),
        ("ws", lambda x: x),
        ("object", lambda x: x),
        ("id", lambda x: x),
        ("lbracket", lambda x: x),
        ("rbracket", lambda x: x),
    ],
    "<text_cont>": [
        ("<text>", lambda xs: xs),
        ("@", lambda: ""),
    ],
}

def parse(input):
    tokens = bnf.parse_tokens(scannerspec)
    rules = bnf.parse_rules(rulespecs)
    bnf.validate_rules(rules, tokens)
    bnf.parse(input, rules, tokens, "<spec>")
    def fixends(ev):
        src = ev.src or Object("none", None, 0)
        dst = ev.dst or Object("none", None, len(objects) - 1)
        return(ev._replace(src=src, dst=dst))
    global events
    events = [fixends(ev) for ev in events]
