Sideseq is a sequence diagram generator that produces fancy HTML diagrams from
text specification. It is designed to allow for long text labels for all 
events/messages.