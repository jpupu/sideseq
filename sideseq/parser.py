from collections import OrderedDict, namedtuple

from sideseq import bnf

Event = namedtuple("Event", "type src dst flair text")
Object = namedtuple("Object", "name text index")


class State(object):

    def __init__(self):
        self.objects = OrderedDict()
        self.events = []
        self.aliases = {}

    def lookup_alias(self, name):
        if name not in self.aliases:
            if name in self.objects:
                return name
            raise bnf.ParseError(
                "Alias target not an object and not an alias: '{}'".format(name)
            )
        return self.lookup_alias(self.aliases[name][-1])

    def lookup_object(self, name):
        return self.objects[self.lookup_alias(name)]

    def cb_object_stmt(self, name, text):
        self.objects[name] = Object(name, text, len(self.objects))

    def cb_alias_object_stmt(self, alias, name):
        actual_name = self.lookup_alias(name)
        self.aliases.setdefault(alias, []).append(actual_name)

    def cb_unalias_object_stmt(self, alias):
        try:
            self.aliases[alias].pop()
            if not self.aliases[alias]:
                del self.aliases[alias]
        except KeyError:
            pass

    def cb_event_flair_stmt(self, name, src, dst, flairy_text):
        flair, text = flairy_text
        if dst in ("-", "."):
            dst = src
        if src == dst and src in ("-", "."):
            srcobj, dstobj = None, None
        else:
            try:
                srcobj, dstobj = self.lookup_object(src), self.lookup_object(dst)
            except KeyError as ex:
                raise bnf.ParseError(
                    "Referenced unknown object '{}' in event ({}, {}, {}, {})".format(
                        ex.args[0], name, src, dst, flairy_text
                    )
                )
        self.events.append(Event(name, srcobj, dstobj, flair, text))

    def scannerspec(self):
        return OrderedDict(
            (
                ("nl", r"\r?\n"),
                ("ws", r"\s+"),
                ("lbracket", r"\["),
                ("rbracket", r"\]"),
                ("hash", r"#"),
                ("alias-object", r"alias-object"),
                ("unalias-object", r"unalias-object"),
                ("object", r"object"),
                ("id", r"[\w\-.]+"),
                ("word", r"\S+"),
            )
        )

    def rulespecs(self):
        return {
            "<spec>": [("<line_list>", None)],
            "<line_list>": [
                ("nl <line_list>", None),
                ("ws <line_list>", None),
                ("<line> <line_list>", None),
                ("@", None),
            ],
            "<line>": [("hash <text>", None), ("<statement>", None)],
            "<statement>": [
                ("<object_stmt>", None),
                ("<alias_object_stmt>", None),
                ("<unalias_object_stmt>", None),
                ("<event_stmt>", None),
            ],
            "<object_stmt>": [("(object) (ws) id (ws) <text>", self.cb_object_stmt)],
            "<alias_object_stmt>": [
                ("(alias-object) (ws) id (ws) <text>", self.cb_alias_object_stmt)
            ],
            "<unalias_object_stmt>": [
                ("(unalias-object) (ws) id", self.cb_unalias_object_stmt)
            ],
            "<event_stmt>": [
                ("id (ws) id (ws) id (ws) <flairy_text>", self.cb_event_flair_stmt)
            ],
            "<flairy_text>": [
                ("<flair> (ws) <text>", None), ("<text>", lambda x: (None, x))
            ],
            "<flair>": [("(lbracket) id (rbracket)", lambda xs: xs)],
            "<text>": [("<text_item> <text_cont>", lambda x, xs: x + xs)],
            "<text_item>": [
                ("word", lambda x: x),
                ("ws", lambda x: x),
                ("object", lambda x: x),
                ("id", lambda x: x),
                ("lbracket", lambda x: x),
                ("rbracket", lambda x: x),
            ],
            "<text_cont>": [("<text>", lambda xs: xs), ("@", lambda: "")],
        }


def parse(input):
    state = State()
    tokens = bnf.parse_tokens(state.scannerspec())
    rules = bnf.parse_rules(state.rulespecs())
    bnf.validate_rules(rules, tokens)
    bnf.parse(input, rules, tokens, "<spec>")

    def fixends(ev):
        src = ev.src or Object("none", None, 0)
        dst = ev.dst or Object("none", None, len(state.objects) - 1)
        return (ev._replace(src=src, dst=dst))

    state.events = [fixends(ev) for ev in state.events]

    return state.objects.values(), state.events
