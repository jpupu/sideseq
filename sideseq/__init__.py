from __future__ import print_function


import sideseq.generator
import sideseq.parser


def process(source, generator, strip=False):
    objects, events = sideseq.parser.parse(source)
    # objects = sideseq.parser.objects.values()
    # events = sideseq.parser.events

    if strip:
        # Find all referenced objects
        refs = []
        for x in events:
            refs += x.src, x.dst
        # Discard any not referenced
        objects = [x for x in objects if x in refs]
        # Fix indices
        for i, x in enumerate(objects):
            x.index = i

    return generator.generate(objects, events)


