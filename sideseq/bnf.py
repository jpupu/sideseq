from __future__ import print_function

import re
from collections import OrderedDict, namedtuple

import more_itertools


class Error(Exception):
    pass


class GrammarError(Error):
    pass


class ParseError(Error):
    pass


Production = namedtuple("Production", "terms action start_set collect_indices")


Rule = namedtuple("Rule", "name productions")


Token = namedtuple("Token", "name value lineno column linetext")


def parse_tokens(tokenspecs):
    return OrderedDict((name, re.compile(spec)) for name, spec in tokenspecs.items())


def parse_rules(rulespecs):
    rules = {}
    for rulename, prodspecs in rulespecs.items():
        prods = []
        for expr, action in prodspecs:
            terms = []
            collect_indices = []
            for index, termspec in enumerate(expr.split()):
                if termspec.startswith("(") and termspec.endswith(")"):
                    termspec = termspec[1:-1]
                else:
                    collect_indices.append(index)
                terms.append(termspec)
            prods.append(Production(tuple(terms), action, None, tuple(collect_indices)))
        rules[rulename] = Rule(rulename, tuple(prods))

    rules = {
        rule.name: Rule(
            rule.name,
            tuple(
                prod._replace(start_set=find_production_start_set(prod, rules))
                for prod in rule.productions
            ),
        )
        for rule in rules.values()
    }
    return rules


def validate_rules(rules, tokens):
    missing_rules = set()
    missing_terminals = set()
    for rule in rules.values():
        for prod in rule.productions:
            for term in prod.terms:
                if is_symbol(term) and term not in rules:
                    missing_rules.add(term)
                elif is_terminal(term) and term not in tokens and term != "@":
                    missing_terminals.add(term)
    if missing_rules:
        raise GrammarError("Missing rules for {}".format(missing_rules))
    if missing_terminals:
        raise GrammarError("Missing tokens for terminals {}".format(missing_terminals))


def is_symbol(term):
    return term.startswith("<") and term.endswith(">")


def is_terminal(term):
    return not is_symbol(term)


def find_rule_start_set(rule, rules):
    start_list = []
    for prod in rule.productions:
        start_list += find_production_start_set(prod, rules)
    start_set = set(start_list)
    if len(start_list) != len(start_set):
        raise GrammarError(
            "Rule {} has duplicates in start set: {}".format(rule.name, start_list)
        )
    return start_set


def find_production_start_set(prod, rules):
    term = prod.terms[0]
    if is_terminal(term):
        return set([term])
    else:
        return find_rule_start_set(rules[term], rules)


def scan(text, tokens):
    pos = 0
    while pos < len(text):
        token = None
        for name, reg in tokens.items():
            mo = reg.match(text, pos)
            if mo:
                linestart = text.rfind("\n", 0, pos) + 1
                lineend = text.find("\n", pos)
                lineno = text.count("\n", 0, pos) + 1
                pos = mo.end()
                token = Token(
                    name, mo.group(0), lineno, pos - linestart, text[linestart:lineend]
                )
                break
        if token:
            yield token
        else:
            print("Invalid token!")
            print("[{}]".format(text[pos:pos + 20]))
            return
    yield Token("EOF", "", None, None, None)


def parse(text, rules, tokens, start_symbol):
    input = more_itertools.peekable(scan(text, tokens))
    result = _parse_rec(input, rules, start_symbol)
    try:
        next_token = next(input)
        if next_token.name != "EOF":
            raise ParseError("Junk after end of program: {}".format(next_token))
    except StopIteration:
        pass
    return result


def _parse_rec(input, rules, symbol):
    rule = rules[symbol]
    # Choose a production of the rule.
    prod = None
    initial_token = input.peek()
    for prodcand in rule.productions:
        if initial_token.name in prodcand.start_set or prodcand.start_set == {"@"}:
            prod = prodcand
            break
    if not prod:
        raise ParseError("No matching production for {}".format(initial_token))

    result = []
    for index, term in enumerate(prod.terms):
        if term == "@":
            continue
        elif is_terminal(term):
            token = next(input)
            if term != token.name:
                raise ParseError(
                    "Expecting {}, got {} while parsing term {} in {} := {}.".format(
                        term, token, index + 1, symbol, " ".join(prod.terms)
                    )
                )
            value = token.value
        else:
            value = _parse_rec(input, rules, term)

        if index in prod.collect_indices:
            result.append(value)

    if prod.action:
        return prod.action(*result)
    else:
        return result


def main():

    def cb_object_stmt(*tokens):
        print("CB_OBJECT_STMT: {}".format(tokens))

    tokenspecs = OrderedDict(
        (("newline", r"\r?\n"), ("ws", r"\s+"), ("object", r"object"), ("word", r"\w+"))
    )

    rulespecs = {
        "<spec>": [("<statement_list>", None)],
        "<statement_list>": [
            ("(newline) <statement_list>", None),
            ("<statement> (newline) <statement_list>", None),
            ("@", None),
        ],
        "<statement>": [("<object_stmt>", None), ("<event_stmt>", None)],
        "<object_stmt>": [("(object) (ws) word (ws) <text>", cb_object_stmt)],
        "<event_stmt>": [("word (ws) word (ws) word (ws) <text>", None)],
        "<text>": [("<text_item> <text_cont>", lambda x, xs: x + xs)],
        "<text_item>": [
            ("word", lambda x: x), ("ws", lambda x: x), ("object", lambda x: x)
        ],
        "<text_cont>": [("<text>", lambda xs: xs), ("@", lambda: "")],
    }

    try:
        tokens = parse_tokens(tokenspecs)
        rules = parse_rules(rulespecs)
        validate_rules(rules, tokens)

        parse(
            """
object A Line A of object X
object B Line B old line C

msg A B   important objectifying message
""",
            rules,
            tokens,
            "<spec>",
        )
    except GrammarError as ex:
        print("!!!!")
        print("error: {}".format(ex))
    except ParseError as ex:
        print("!!!!")
        print("error: {}".format(ex))


if __name__ == "__main__":
    main()
