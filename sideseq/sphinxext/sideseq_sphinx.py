"""Sideseq extension for Sphinx doc."""
from docutils import nodes
from docutils.parsers.rst import Directive, directives
from docutils.statemachine import ViewList

from .. import generator, process


def setup(app):
    app.add_directive("sideseq", SideseqDirective)

    return {"version": "0.1"}


class SideseqDirective(Directive):
    has_content = True
    optional_arguments = 1

    option_spec = {"caption": directives.unchanged}

    def run(self):
        gen = generator.HTMLGenerator(as_fragment=True)
        diag_text = process("\n".join(self.content), gen)
        diag_node = nodes.raw("", diag_text, format="html")

        figure_node = nodes.figure("")

        caption = self.options.get("caption")
        if caption:
            # We do this stuff so the caption is parsed as ReST.
            parsed = nodes.Element()
            self.state.nested_parse(
                ViewList([caption], source=""), self.content_offset, parsed
            )
            caption_node = nodes.caption(parsed[0].rawsource, "", *parsed[0].children)
            caption_node.source = diag_node.source
            caption_node.line = diag_node.line
            # (If we only did this, it would not be ReST.)
            # caption_node = nodes.caption('', self.options['caption'])

            figure_node += caption_node

        figure_node += diag_node
        return [figure_node]
