from __future__ import print_function

import argparse
import io
import os
import sys

import jinja2

from sideseq import generator, process


class Sequence(object):

    def __init__(self, kind):
        self.base_stack = ["__main__"]
        self.counter = 0
        self.history = {}
        self.kind = kind

    def __call__(self, ofs):
        return self.get(ofs)

    def push(self):
        self.counter += 1
        self.base_stack.append(self.counter)

    def pop(self):
        self.base_stack.pop()

    def get(self, ofs):
        seqid = (self.base_stack[-1], ofs)
        if seqid not in self.history:
            self.history[seqid] = len(self.history)
        return self._render(self.history[seqid])

    def _render(self, index):
        if self.kind == "greek":
            cycle = "αβγδεζηθικλμνξοπρστυφχψωΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ"
            return cycle[index % len(cycle)]
        return index


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", default="-", nargs="?")
    parser.add_argument(
        "-g", "--generator", choices=("html", "html-fragment", "ascii"), default="html"
    )
    parser.add_argument(
        "--css-file",
        type=argparse.FileType("r"),
        help="inject additional css file's contents",
    )
    parser.add_argument("-o", "--outfile")
    parser.add_argument(
        "-T",
        "--template-only",
        action="store_true",
        help="Only render template, output is generated seq source text.",
    )
    parser.add_argument(
        "-d",
        "--define",
        metavar="NAME[=VALUE]",
        action="append",
        default=[],
        type=lambda x: x.split("=", 1) if "=" in x else (x, True),
        help="Define variables passed to Jinja2 templates. "
        "The value defaults to True if not given.",
    )
    parser.add_argument(
        "-s", "--strip-objects", action="store_true", help="Strip unused objects"
    )
    parser.add_argument(
        "-n",
        "--number-events",
        action="store_true",
        help="Write event number to diagram.",
    )
    args = parser.parse_args()

    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader("."), extensions=["jinja2.ext.do"]
    )
    env.globals = {"get_sequence": Sequence}
    env.globals.update(dict(args.define))
    if args.infile == "-":
        infile = io.TextIOWrapper(sys.stdin.buffer, encoding="utf-8")
        template = env.from_string(infile.read())
    else:
        template = env.get_template(args.infile)
    text = template.render()

    if args.template_only:
        print(text)
        return

    if args.generator == "html":
        gen = generator.HTMLGenerator(
            as_fragment=False,
            extra_css=args.css_file.read() if args.css_file else "",
            number_events=args.number_events,
        )
    elif args.generator == "html-fragment":
        gen = generator.HTMLGenerator(
            as_fragment=True,
            extra_css=args.css_file.read() if args.css_file else "",
            number_events=args.number_events,
        )
    elif args.generator == "ascii":
        gen = generator.ASCIIGenerator(number_events=args.number_events)

    outtext = process(text, gen)

    if args.outfile == "-" or (not args.outfile and args.infile == "-"):
        outfile = io.TextIOWrapper(sys.stdout.buffer, encoding="utf-8")
    elif not args.outfile:
        if args.generator in ("html", "html-fragment"):
            ext = ".html"
        elif args.generator == "ascii":
            ext = ".txt"
        outfile = open(os.path.splitext(args.infile)[0] + ext, "w", encoding="utf-8")
    else:
        outfile = open(args.outfile, "w", encoding="utf-8")
    outfile.write(outtext)
    outfile.close()


if __name__ == "__main__":
    main()
