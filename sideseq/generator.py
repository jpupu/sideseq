from __future__ import print_function

import math
import textwrap
from collections import namedtuple

EventType = namedtuple(
    "EventType",
    ["name", "shaft", "tail", "head", "tail_extend", "head_extend", "reverse_color"],
)

types = {
    "msg": EventType("msg", "line", "circle", "arrow", 0, -2, False),
    "reply": EventType("reply", "line", "circle", "arrow", 0, -2, True),
    "divider": EventType("divider", "line", None, None, 10, 10, False),
    "note": EventType("note", "box", None, None, 10, 10, False),
    "void": EventType("void", None, None, None, 0, 0, False),
    "act": EventType("act", None, "lcircle", None, 0, 0, False),
}


class HTMLGenerator(object):
    STYLE = textwrap.dedent(
        u"""
    div.diag {
        display: inline-block;
    }
    div.event-index {
        display: inline-block;
        vertical-align: top;
        font-size: xx-small;
        font-family: "Arial","Helvetica";
    }
    div.event-text {
        display: inline-block;
        vertical-align: top;
    }
    div.event-flair {
        display: inline-block;
        vertical-align: top;
        margin-right: 1ex;

        color: #fff;
        background-color: #555;
        border: 0;
        border-radius: 15%;
        cursor: text;
        font-size: x-small;
        font-weight: bold;
        font-family: "Arial","Helvetica";
        margin: 2px 4px 0 0;
        padding: 1px 3px;
    }

    .event {
        fill: none;
        stroke-width: 2;
        stroke: black;
    }

    .type-reply .shaft {
        stroke-dasharray: 5 5;
    }
    .type-divider .shaft {
        stroke-dasharray: 3 6;
    }

    .event .arrow {
        fill: none;
    }

    .event .box {
        fill: white;
    }

    .lifeline {
        border-left: 1px dashed gray;
    }
    .lifeline-label {
        font-size: 90%;
    }
    span.lifeline-label {
        display: inline-block;
        transform: rotate(-45deg);
        transform-origin: bottom left;
        white-space: nowrap;
        border-bottom: 1px solid gray;
    }
    """
    )

    HEADER = textwrap.dedent(
        u"""
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset="utf-8" />
    <style>
    {style}
    {autocolor_style}
    {extra_style}
    </style>
    </head>
    <body>
    """
    )

    FOOTER = textwrap.dedent(
        u"""
    </body>
    </html>
    """
    )

    def __init__(self, as_fragment, extra_css="", number_events=False):
        self.as_fragment = as_fragment
        self.extra_style = extra_css
        self.number_events = number_events

    def generate(self, objects, events):
        self.objects = objects
        self.events = events
        self.OBJECT_W = 30
        self.TOTAL_W = len(objects) * self.OBJECT_W
        self.EVENT_H = 20

        self.autocolor_style = ""
        palette = [
            "#1b9e77",
            "#d95f02",
            "#7570b3",
            "#e7298a",
            "#66a61e",
            "#e6ab02",
            "#a6761d",
            "#666666",
        ]
        for i, obj in enumerate(objects):
            col = palette[i % len(palette)]
            self.autocolor_style += u".color-from-{obj} {{ color: {col}; stroke: {col} }}\n".format(
                obj=obj.name, col=col
            )
            self.autocolor_style += u".lifeline-of-{obj} {{ color: {col}; border-color: {col}; }}\n".format(
                obj=obj.name, col=col
            )
            self.autocolor_style += u".color-from-{obj} .circle {{ fill: {col}; }}\n".format(
                obj=obj.name, col=col
            )

        if self.as_fragment:
            out = self.render_fragment()
        else:
            out = self.render_page()
        return "\n".join(out)

    def event(self, event_index, ev):
        out = []
        out.append(
            u"""<div class="event type-{type} from-{src} to-{dst} color-from-{color_src}" ><div class="diag" style="width:{total_w}px;"><svg width="{total_w}" height="{event_h}">""".format(
                total_w=self.TOTAL_W,
                event_h=self.EVENT_H,
                src=ev.src.name,
                dst=ev.dst.name,
                type=types[ev.type].name,
                color_src=(ev.dst if types[ev.type].reverse_color else ev.src).name,
            )
        )
        reverse = ev.dst.index < ev.src.index

        x1 = (ev.src.index + 0.5) * self.OBJECT_W - types[ev.type].tail_extend * (
            -1 if reverse else 1
        )
        x2 = (ev.dst.index + 0.5) * self.OBJECT_W + types[ev.type].head_extend * (
            -1 if reverse else 1
        )
        y = self.EVENT_H / 2

        if types[ev.type].shaft == "line":
            out.append(
                """<line class="shaft line" x1="{x1}" y1="{y}" x2="{x2}" y2="{y}"></line>""".format(
                    x1=x1, x2=x2, y=y
                )
            )
        elif types[ev.type].shaft == "box":
            y1 = self.EVENT_H * 0.14
            y2 = self.EVENT_H * 0.85
            out.append(
                """<rect class="shaft box" x="{x}" y="{y}" width="{w}" height="{h}"></rectangle>""".format(
                    x=x1, y=y1, w=x2 - x1, h=y2 - y1
                )
            )

        if types[ev.type].head == "arrow":
            head_h = 6
            head_w = -8 if reverse else 8
            out.append(
                """<polyline class="head arrow" points="{},{} {},{} {},{}"></polyline>""".format(
                    x2 - head_w, y - head_h, x2, y, x2 - head_w, y + head_h
                )
            )
        if types[ev.type].tail == "arrow":
            head_h = 6
            head_w = 8 if reverse else -8
            out.append(
                """<polyline class="tail arrow" points="{},{} {},{} {},{}"></polyline>""".format(
                    x1 - head_w, y - head_h, x1, y, x1 - head_w, y + head_h
                )
            )
        if types[ev.type].head == "circle":
            out.append(
                """<circle class="head circle" cx="{}" cy="{}" r="{}" />""".format(
                    x2, y, 2
                )
            )
        if types[ev.type].tail == "circle":
            out.append(
                """<circle class="tail circle" cx="{}" cy="{}" r="{}" />""".format(
                    x1, y, 2
                )
            )
        if types[ev.type].head == "lcircle":
            out.append(
                """<circle class="head circle" cx="{}" cy="{}" r="{}" />""".format(
                    x2, y, 3
                )
            )
        if types[ev.type].tail == "lcircle":
            out.append(
                """<circle class="tail circle" cx="{}" cy="{}" r="{}" />""".format(
                    x1, y, 3
                )
            )

        out.append("""</svg></div>""")
        if self.number_events:
            out.append(u"""<div class="event-index">{}</div>""".format(event_index + 1))
        if ev.flair:
            out.append(
                u"""<div class="event-flair flair-{0}">{0}</div>""".format(ev.flair)
            )
        out.append(u"""<div class="event-text">{}</div></div>""".format(ev.text))
        return out

    def render_diagram(self):
        out = []
        # Labels.
        max_label_length = max(len(o.text) for o in self.objects)
        label_height = max_label_length * math.sin(45 * math.pi / 180)
        out.append(
            """<div class="lifeline-label" style="height:{:.1f}em; position:relative;">""".format(
                label_height
            )
        )
        for i, o in enumerate(self.objects):
            out.append(
                u"""<div style="position:absolute; left:{x}px; bottom:0px; width:{w}px;"><span class="lifeline-label lifeline-of-{objref}">{name}</span></div>""".format(
                    x=(i + .5) * self.OBJECT_W + 1,
                    w=self.OBJECT_W,
                    name=o.text,
                    objref=o.name,
                )
            )
        out.append("""</div>""")

        # Lifelines and events.
        out.append("""<div style="position:relative;">""")

        for i, o in enumerate(self.objects):
            out.append(
                u"""<div class="lifeline lifeline-of-{objref}" style="position:absolute; left:{x}px; top:0px; width:1px; height:100%;"></div>""".format(
                    x=(i + .5) * self.OBJECT_W, objref=o.name
                )
            )

        for i, m in enumerate(self.events):
            out.extend(self.event(i, m))

        out.append("""</div>""")
        return out

    def render_fragment(self):
        out = []
        out.append("""<!-- Begin sideseq diagram -->""")
        out.append("""<div class="sideseq-diagram"><style scoped>""")
        out.append(self.STYLE)
        out.append(self.autocolor_style)
        out.append("""</style>""")
        out.extend(self.render_diagram())
        out.append("""</div>""")
        out.append("""<!-- End sideseq diagram -->""")
        return out

    def render_page(self):
        out = []
        out.append(
            self.HEADER.format(
                style=self.STYLE,
                autocolor_style=self.autocolor_style,
                extra_style=self.extra_style,
            )
        )
        out.extend(self.render_diagram())
        out.append(self.FOOTER)
        return out


class ASCIIGenerator(object):

    def __init__(self, number_events=False):
        self.number_events = number_events

    def generate(self, objects, events):
        out = []
        for i, obj in enumerate(objects):
            out.append(" |  " * i + obj.text)

        for event_index, ev in enumerate(events):
            line = list(" |  " * len(objects))
            rev = ev.src.index > ev.dst.index
            x1, x2 = (ev.src.index, ev.dst.index) if not rev else (
                ev.dst.index, ev.src.index
            )
            x1 = x1 * 4 + 1
            x2 = x2 * 4 + 1

            if types[ev.type].shaft == "line":
                for x in range(x1 + 1, x2):
                    line[x] = "-"
            elif types[ev.type].shaft == "box":
                line[x1 - 1] = "["
                line[x2 + 1] = "]"
                for x in range(x1, x2 + 1):
                    line[x] = "_"

            if types[ev.type].head == "arrow":
                if rev:
                    line[x1 + 1] = "<"
                else:
                    line[x2 - 1] = ">"

            if types[ev.type].tail == "arrow":
                if rev:
                    line[x2 - 1] = ">"
                else:
                    line[x1 + 1] = "<"
            line = "".join(line)
            if ev.flair:
                line += "[" + ev.flair + "] "
            if self.number_events:
                line += "{}. ".format(event_index + 1)
            line += ev.text
            out.append(line)
        return "\n".join(out + [""])
